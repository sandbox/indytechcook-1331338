<?php

namespace Drupal\system\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DerivativeInterface;

class SystemMenuBlock implements DerivativeInterface {
  protected $derivatives;

  /**
   * Implements DerivativeInterface::getDerivativeDefinition().
   */
  public function getDerivativeDefinition($derivative_id, array $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $this->getDerivativeDefinitions($base_plugin_definition);
    return $this->derivatives[$derivative_id];
  }

  /**
   * Implements DerivativeInterface::getDerivativeDefinitions().
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    foreach (menu_list_system_menus() as $menu => $name) {
      $this->derivatives[$menu] = $base_plugin_definition;
      $this->derivatives[$menu]['delta'] = $menu;
      $this->derivatives[$menu]['subject'] = t('System Menu: @menu', array('@menu' => $name));
      $this->derivatives[$menu]['cache'] = DRUPAL_NO_CACHE;
    }
    return $this->derivatives;
  }
}
