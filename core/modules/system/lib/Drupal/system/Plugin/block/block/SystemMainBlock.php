<?php

namespace Drupal\system\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "system_main_block",
 *   subject = @Translation("Main page content"),
 *   module = "system",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class SystemMainBlock extends BlockBase {

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    return array(
      drupal_set_page_content()
    );
  }
}
