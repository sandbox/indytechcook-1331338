<?php

namespace Drupal\system\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "system_menu_block",
 *   subject = @Translation("System Menu"),
 *   module = "system",
 *   derivative = "Drupal\system\Plugin\Derivative\SystemMenuBlock",
 *   settings = {
 *     "status" = "TRUE"
 *   }
 * )
 */
class SystemMenuBlock extends BlockBase {

  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    global $user;
    if (!$user->uid) {
      return FALSE;
    }
    return parent::access();
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    list($plugin, $derivative) = explode(':', $this->getPluginId());
    return menu_tree($derivative);
  }
}
