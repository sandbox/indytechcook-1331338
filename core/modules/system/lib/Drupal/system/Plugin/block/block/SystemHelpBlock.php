<?php

namespace Drupal\system\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "system_help_block",
 *   subject = @Translation("System Help"),
 *   module = "system",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class SystemHelpBlock extends BlockBase {
  private $help;

  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    $this->help = menu_get_active_help();
    return $this->help ? TRUE : FALSE;
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    if (empty($this->help)) {
      $this->help = menu_get_active_help();
    }
    return array(
      '#children' => $this->help,
    );
  }
}
