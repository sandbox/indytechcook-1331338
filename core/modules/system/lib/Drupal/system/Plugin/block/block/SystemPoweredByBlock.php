<?php

namespace Drupal\system\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "system_powered_by_block",
 *   subject = @Translation("Powered by Drupal"),
 *   module = "system",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class SystemPoweredByBlock extends BlockBase {

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    return array(
      '#children' => theme('system_powered_by'),
    );
  }
}
