<?php

namespace Drupal\poll\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "poll_recent_block",
 *   subject = @Translation("Most recent poll"),
 *   module = "poll",
 *   settings = {
 *     "status" = TRUE,
 *     "properties" = {
 *       "administrative" = TRUE
 *     }
 *   }
 * )
 */
class PollRecentBlock extends BlockBase {
  protected $record;

  /**
   * Implements BlockInterface::access().
   */
  function access() {
    if (user_access('access content')) {
      // Retrieve the latest poll.
      $select = db_select('node', 'n');
      $select->join('poll', 'p', 'p.nid = n.nid');
      $select->fields('n', array('nid'))
        ->condition('n.status', 1)
        ->condition('p.active', 1)
        ->orderBy('n.created', 'DESC')
        ->range(0, 1)
        ->addTag('node_access');

      $record = $select->execute()->fetchObject();
      if ($record) {
        $this->record = $record;
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Implements BlockInterface::build().
   */
  function build() {
    $poll = node_load($this->record->nid);
    if ($poll->nid) {
      $poll = poll_block_latest_poll_view($poll);
      return array(
        $poll->content
      );
    }
    return array();
  }
}