<?php

namespace Drupal\search\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "search_form_block",
 *   subject = @Translation("Search form"),
 *   module = "search",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class SearchBlock extends BlockBase {

  /**
   * Implements BlockInterface::access().
   */
  public function access() {
    return user_access('search content');
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    return drupal_get_form('search_block_form');
  }
}
