<?php

namespace Drupal\shortcut\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *  id = "shortcuts",
 *  subject = @Translation("Shortcuts"),
 *  module = "shortcut",
 *  settings = {
 *    "status" = TRUE,
 *    "cache" = DRUPAL_NO_CACHE
 *  }
 * )
 */
class ShortcutsBlock extends BlockBase {
  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    $shortcut_set = shortcut_current_displayed_set();
    return array(
      shortcut_renderable_links($shortcut_set),
    );
  }
}
