<?php

namespace Drupal\language\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DerivativeInterface;

class LanguageBlock implements DerivativeInterface {
  protected $derivatives;

  /**
   * Implements DerivativeInterface::getDerivativeDefinition().
   */
  public function getDerivativeDefinition($derivative_id, array $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $this->getDerivativeDefinitions($base_plugin_definition);
    return $this->derivatives[$derivative_id];
  }

  /**
   * Implements DerivativeInterface::getDerivativeDefinitions().
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    include_once DRUPAL_ROOT . '/core/includes/language.inc';
    $info = language_types_info();
    foreach (language_types_get_configurable(FALSE) as $type) {
      $this->derivatives[$type] = $base_plugin_definition;
      $this->derivatives[$type]['subject'] = t('Language switcher (!type)', array('!type' => $info[$type]['name']));
      $this->derivatives[$type]['cache'] = DRUPAL_NO_CACHE;
    }
    return $this->derivatives;
  }
}
