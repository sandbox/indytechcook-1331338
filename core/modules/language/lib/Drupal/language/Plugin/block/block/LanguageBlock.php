<?php

namespace Drupal\language\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "language_block",
 *   subject = @Translation("Language switcher"),
 *   module = "language",
 *   derivative = "Drupal\language\Plugin\Derivative\LanguageBlock",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class LanguageBlock extends BlockBase {
  function access() {
    return language_multilingual();
  }

  function build() {
    $path = drupal_is_front_page() ? '<front>' : current_path();
    list($plugin_id, $type) = explode(':', $this->getPluginId());
    $links = language_negotiation_get_switch_links($type, $path);

    if (isset($links->links)) {
      $class = "language-switcher-{$links->method_id}";
      $variables = array('links' => $links->links, 'attributes' => array('class' => array($class)));
      return array(
        '#children' => theme('links__language_block', $variables)
      );
    }
  }
}
