<?php

namespace Drupal\user\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "user_login_block",
 *   subject = @Translation("User login"),
 *   module = "user",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE,
 *     "whois_new_count" = 5
 *   }
 * )
 */
class UserLoginBlock extends BlockBase {
  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    global $user;
    return (!$user->uid && !(arg(0) == 'user' && !is_numeric(arg(1))));
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    return array(
      drupal_get_form('user_login_block')
    );
  }
}
