<?php

namespace Drupal\user\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "user_online_block",
 *   subject = @Translation("Who's new"),
 *   module = "user",
 *   settings = {
 *     "status" = TRUE,
 *     "properties" = {
 *       "administrative" = TRUE
 *     },
 *     "whois_new_count" = 5
 *   }
 * )
 */
class UserNewBlock extends BlockBase {
  /**
   * Implements BlockInterface::access().
   */
  public function access() {
    return user_access('access content');
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $form = parent::configure($form, $form_state);
    $form['user_block_whois_new_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of users to display'),
      '#default_value' => $this->configuration['whois_new_count'],
      '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    );
    return $form;
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    parent::configureSubmit($form, $form_state);
    $this->configuration['whois_new_count'] = $form_state['values']['user_block_whois_new_count'];
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    // Retrieve a list of new users who have subsequently accessed the site successfully.
    $items = db_query_range('SELECT uid, name FROM {users} WHERE status <> 0 AND access <> 0 ORDER BY created DESC', 0, $this->configuration['whois_new_count'])->fetchAll();
    return array(
      '#children' => theme('user_list', array('users' => $items)),
    );
  }
}
