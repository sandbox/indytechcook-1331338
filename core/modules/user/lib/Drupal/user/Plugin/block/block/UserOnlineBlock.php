<?php

namespace Drupal\user\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "user_online_block",
 *   subject = @Translation("Who's online"),
 *   module = "user",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_NO_CACHE,
 *     "properties" = {
 *       "administrative" = TRUE
 *     },
 *     "seconds_online" = 900,
 *     "max_list_count" = 10
 *   }
 * )
 */
class UserOnlineBlock extends BlockBase {
  /**
   * Implements BlockInterface::access().
   */
  public function access() {
    return user_access('access content');
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $form = parent::configure($form, $form_state);
    $period = drupal_map_assoc(array(30, 60, 120, 180, 300, 600, 900, 1800, 2700, 3600, 5400, 7200, 10800, 21600, 43200, 86400), 'format_interval');
    $form['user_block_seconds_online'] = array(
      '#type' => 'select',
      '#title' => t('User activity'),
      '#default_value' => $this->configuration['seconds_online'],
      '#options' => $period,
      '#description' => t('A user is considered online for this long after they have last viewed a page.')
    );
    $form['user_block_max_list_count'] = array(
      '#type' => 'select',
      '#title' => t('User list length'),
      '#default_value' => $this->configuration['max_list_count'],
      '#options' => drupal_map_assoc(array(0, 5, 10, 15, 20, 25, 30, 40, 50, 75, 100)),
      '#description' => t('Maximum number of currently online users to display.')
    );
    return $form;
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    parent::configureSubmit($form, &$form_state);
    $this->configuration['seconds_online'] = $form_state['values']['user_block_seconds_online'];
    $this->configuration['max_list_count'] = $form_state['values']['user_block_max_list_count'];
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    // Count users active within the defined period.
    $interval = REQUEST_TIME - $this->configuration['max_list_count'];

    // Perform database queries to gather online user lists. We use s.timestamp
    // rather than u.access because it is much faster.
    $authenticated_count = db_query("SELECT COUNT(DISTINCT s.uid) FROM {sessions} s WHERE s.timestamp >= :timestamp AND s.uid > 0", array(':timestamp' => $interval))->fetchField();

    $output = '<p>' . format_plural($authenticated_count, 'There is currently 1 user online.', 'There are currently @count users online.') . '</p>';

    // Display a list of currently online users.
    $max_users = $this->configuration['max_list_count'];
    if ($authenticated_count && $max_users) {
      $items = db_query_range('SELECT u.uid, u.name, MAX(s.timestamp) AS max_timestamp FROM {users} u INNER JOIN {sessions} s ON u.uid = s.uid WHERE s.timestamp >= :interval AND s.uid > 0 GROUP BY u.uid, u.name ORDER BY max_timestamp DESC', 0, $max_users, array(':interval' => $interval))->fetchAll();
      $output .= theme('user_list', array('users' => $items));
    }

    return array(
      '#children' => $output,
    );
  }
}
