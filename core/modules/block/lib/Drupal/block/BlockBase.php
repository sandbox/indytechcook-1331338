<?php

namespace Drupal\block;

use Drupal\Component\Plugin\PluginBase;
use Symfony\Component\Routing\RequestContext;

/**
 * @file
 *   An abstract block implementation to be inherited from to take care of many
 *   common block setup tasks.
 */
abstract class BlockBase extends PluginBase implements BlockInterface {
  protected $context;

  /**
   * Implements BlockInterface::getConfig().
   */
  public function getConfig() {
    if (empty($this->configuration)) {
      $definition = $this->getDefinition();
      $this->configuration = $definition['settings'];
    }
    return $this->configuration;
  }

  /**
   * Implements BlockInterface::access().
   */
  public function access() {
    return TRUE;
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $definition = $this->getDefinition();
    $config = $this->getConfig();
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $definition['id'],
    );
    $form['module'] = array(
      '#type' => 'value',
      '#value' => $definition['module'],
    );

    // Get the block subject for the page title.
    $subject = isset($config['subject']) ? $config['subject'] : '';
    if ($subject) {
      drupal_set_title(t("'%subject' block", array('%subject' => $subject)), PASS_THROUGH);
    }

    $form['settings']['machine_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Block machine name'),
      '#maxlength' => 64,
      '#description' => t('A unique name to save this block configuration. Must be alpha-numeric and be underscore separated.'),
      '#default_value' => isset($config['config_id']) ? $config['config_id'] : '',
      '#weight' => -20,
      '#required' => TRUE,
    );
    if (isset($config['config_id'])) {
      $form['settings']['machine_name']['#disabled'] = TRUE;
    }

    $form['settings']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Block title'),
      '#maxlength' => 64,
      '#description' => $definition['module'] == 'block' ? t('The title of the block as shown to the user.') : t('Override the default title for the block. Use <em>!placeholder</em> to display no title, or leave blank to use the default block title.', array('!placeholder' => '&lt;none&gt;')),
      '#default_value' => isset($subject) ? $subject : '',
      '#weight' => -19,
    );

    // Region settings.
    $form['regions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Region settings'),
      '#collapsible' => FALSE,
      '#description' => t('Specify in which themes and regions this block is displayed.'),
    );

    $theme_default = variable_get('theme_default', 'stark');
    $admin_theme = variable_get('admin_theme');
    $themes = list_themes();
    $key = $form['theme']['#value'];
    $theme = $themes[$key];
    // Only display enabled themes
    if ($theme->status) {
      // Use a meaningful title for the main site theme and administrative
      // theme.
      $theme_title = $theme->info['name'];
      if ($key == $theme_default) {
        $theme_title = t('!theme (default theme)', array('!theme' => $theme_title));
      }
      elseif ($admin_theme && $key == $admin_theme) {
        $theme_title = t('!theme (administration theme)', array('!theme' => $theme_title));
      }
      $form['regions']['region'] = array(
        '#type' => 'select',
        '#title' => $theme_title,
        '#default_value' => !empty($config['region']) && $config['region'] != -1 ? $config['region'] : NULL,
        '#empty_value' => BLOCK_REGION_NONE,
        '#options' => system_region_list($key, REGIONS_VISIBLE),
        '#required' => TRUE,
      );
    }

    // Visibility settings.
    $form['visibility_title'] = array(
      '#type' => 'item',
      '#title' => t('Visibility settings'),
    );
    $form['visibility'] = array(
      '#type' => 'vertical_tabs',
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'block') . '/block.js'),
      ),
    );

    // Per-path visibility.
    $form['visibility']['path'] = array(
      '#type' => 'fieldset',
      '#title' => t('Pages'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'visibility',
      '#weight' => 0,
    );

    //@TODO remove this access check and inject it in some other way.
    // In fact this entire visibility settings section probably needs a
    // separate user interface in the near future.
    $access = user_access('use PHP for settings');
    if (isset($block->visibility) && $block->visibility == BLOCK_VISIBILITY_PHP && !$access) {
      $form['visibility']['path']['visibility'] = array(
        '#type' => 'value',
        '#value' => BLOCK_VISIBILITY_PHP,
      );
      $form['visibility']['path']['pages'] = array(
        '#type' => 'value',
        '#value' => isset($block->pages) ? $block->pages : '',
      );
    }
    else {
      $options = array(
        BLOCK_VISIBILITY_NOTLISTED => t('All pages except those listed'),
        BLOCK_VISIBILITY_LISTED => t('Only the listed pages'),
      );
      $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %user for the current user's page and %user-wildcard for every user page. %front is the front page.", array('%user' => 'user', '%user-wildcard' => 'user/*', '%front' => '<front>'));

      if (module_exists('php') && $access) {
        $options += array(BLOCK_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
        $title = t('Pages or PHP code');
        $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
      }
      else {
        $title = t('Pages');
      }
      $form['visibility']['path']['visibility'] = array(
        '#type' => 'radios',
        '#title' => t('Show block on specific pages'),
        '#options' => $options,
        '#default_value' => isset($block->visibility) ? $block->visibility : BLOCK_VISIBILITY_NOTLISTED,
      );
      $form['visibility']['path']['pages'] = array(
        '#type' => 'textarea',
        '#title' => '<span class="element-invisible">' . $title . '</span>',
        '#default_value' => isset($block->pages) ? $block->pages : '',
        '#description' => $description,
      );
    }

    // Per-role visibility.
    $default_role_options = db_query("SELECT rid FROM {block_role} WHERE module = :module", array(
      ':module' => $definition['module'],
    ))->fetchCol();
    $role_options = array_map('check_plain', user_roles());
    $form['visibility']['role'] = array(
      '#type' => 'fieldset',
      '#title' => t('Roles'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'visibility',
      '#weight' => 10,
    );
    $form['visibility']['role']['roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Show block for specific roles'),
      '#default_value' => $default_role_options,
      '#options' => $role_options,
      '#description' => t('Show this block only for the selected role(s). If you select no roles, the block will be visible to all users.'),
    );

    // Per-user visibility.
    $form['visibility']['user'] = array(
      '#type' => 'fieldset',
      '#title' => t('Users'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'visibility',
      '#weight' => 20,
    );
    $form['visibility']['user']['custom'] = array(
      '#type' => 'radios',
      '#title' => t('Customizable per user'),
      '#options' => array(
        BLOCK_CUSTOM_FIXED => t('Not customizable'),
        BLOCK_CUSTOM_ENABLED => t('Customizable, visible by default'),
        BLOCK_CUSTOM_DISABLED => t('Customizable, hidden by default'),
      ),
      '#description' => t('Allow individual users to customize the visibility of this block in their account settings.'),
      '#default_value' => isset($block->custom) ? $block->custom : BLOCK_CUSTOM_FIXED,
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save block'),
    );

    return $form;
  }

  /**
   * Implements BlockInterface::configureValidate().
   */
  public function configureValidate($form, &$form_state) {
    if (empty($form['settings']['machine_name']['#disabled'])) {
      if (preg_match('/[^a-zA-Z0-9_]/', $form_state['values']['machine_name'])) {
        form_set_error('machine_name', t('Block name must be alphanumeric or underscores only.'));
      }
      if (in_array('plugin.core.block.' . $form_state['values']['machine_name'], config_get_storage_names_with_prefix('plugin.core.block'))) {
        form_set_error('machine_name', t('Block name must be unique.'));
      }
    }
    else {
      $config_id = explode('.', $form_state['values']['machine_name']);
      $form_state['values']['machine_name'] = array_pop($config_id);
    }
    if ($form_state['values']['module'] == 'block') {
      $custom_block_exists = (bool) db_query_range('SELECT 1 FROM {block_custom} WHERE bid <> :bid AND info = :info', 0, 1, array(
        ':bid' => $form_state['values']['delta'],
        ':info' => $form_state['values']['info'],
      ))->fetchField();
      if (empty($form_state['values']['info']) || $custom_block_exists) {
        form_set_error('info', t('Ensure that each block description is unique.'));
      }
    }
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    if (!form_get_errors()) {
      $transaction = db_transaction();
      try {
        $keys = array(
          'visibility' => 'visibility',
          'pages' => 'pages',
          'custom' => 'custom',
          'title' => 'subject',
          'module' => 'module',
          'region' => 'region',
        );
        foreach ($keys as $key => $new_key) {
          if (isset($form_state['values'][$key])) {
            $this->configuration[$new_key] = $form_state['values'][$key];
          }
        }
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('block', $e);
        throw $e;
      }
      if (empty($this->configuration['weight'])) {
        $this->configuration['weight'] = 0;
      }
      drupal_set_message(t('The block configuration has been saved.'));
      drupal_flush_all_caches();
    }
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {}
}
