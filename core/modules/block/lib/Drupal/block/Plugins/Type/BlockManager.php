<?php

namespace Drupal\block\Plugins\Type;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\Discovery\DerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\block\Plugins\ConfigMapper;

class BlockManager extends PluginManagerBase {
  public function __construct() {
    $this->discovery = new DerivativeDiscoveryDecorator(new AnnotatedClassDiscovery('block', 'block'));
    $this->factory = new DefaultFactory($this);
    $this->mapper = new ConfigMapper($this);
  }
}