<?php

namespace Drupal\block\Plugins;

use Drupal\Component\Plugin\Mapper\MapperInterface;

class ConfigMapper implements MapperInterface {
  protected $manager;

  public function __construct($manager) {
    $this->manager = $manager;
  }

  /**
   * Implements MapperInterface::getInstance().
   */
  public function getInstance(array $options) {
    $config = config($options['config']);
    if ($config) {
      $plugin_id = $config->get('id');
      $settings = $config->get();
      $settings['config_id'] = $options['config'];
      return $this->manager->createInstance($plugin_id, $settings);
    }
    // @Todo throw an exception.
  }
}