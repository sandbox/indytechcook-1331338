<?php

namespace Drupal\block;

use Drupal\Plugin\Access\AccessInterface;

/**
 * Interface definition for Block plugins.
 */
interface BlockInterface {

  /**
   * A simple access method. This should be replaced with access plugins before
   * Drupal 8 is released.
   *
   * @return bool.
   */
  public function access();

  /**
   * The configuration form for the block.
   *
   * @return form array().
   */
  public function configure($form, &$form_state);

  /**
   * The validation for the configuration form.
   */
  public function configureValidate($form, &$form_state);

  /**
   * The submission for the configuration form.
   */
  public function configureSubmit($form, &$form_state);

  /**
   * A function for building renderable block arrays. A block theme wrapper
   * will be wrapped around this by the caller.
   *
   * @return renderable array().
   */
  public function build();
}
