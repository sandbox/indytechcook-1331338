<?php

/**
 * @file
 * Definition of Drupal\block\BlockBundle.
 */

namespace Drupal\Block;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Block dependency injection container.
 */
class BlockBundle extends Bundle {
  /**
   * Overrides Symfony\Component\HttpKernel\Bundle\Bundle.
   */
  public function build(ContainerBuilder $container) {
    // register the BlockManager class with the dependency injection container.
    $container->register('plugin.manager.block', 'Drupal\block\Plugins\Type\BlockManager');
  }
}