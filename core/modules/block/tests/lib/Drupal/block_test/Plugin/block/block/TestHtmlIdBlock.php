<?php

namespace Drupal\block_test\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "test_html_id",
 *   subject = @Translation("Test block html id"),
 *   module = "block_test",
 *   settings = {
 *     "status" = TRUE,
 *   }
 * )
 */
class TestHtmlIdBlock extends TestCacheBlock {}
