<?php

namespace Drupal\block_test\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Annotation\Evaluate;

/**
 * @Plugin(
 *   id = "test_cache",
 *   subject = @Translation("Test block caching"),
 *   module = "block_test",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = @Evaluate("return variable_get('block_test_caching', DRUPAL_CACHE_PER_ROLE);"),
 *   }
 * )
 */
class TestCacheBlock extends BlockBase {
  public function build() {
    return array(variable_get('block_test_content', ''));
  }
}
