<?php

namespace Drupal\forum\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "forum_active_block",
 *   subject = @Translation("New forum topics"),
 *   module = "forum",
 *   settings = {
 *     "status" = TRUE,
 *     "cache" = DRUPAL_CACHE_CUSTOM,
 *     "properties" = {
 *       "administrative" = TRUE
 *     },
 *     "block_count" = 5
 *   }
 * )
 */
class NewBlock extends BlockBase {

  /**
   * Implements BlockInterface::access().
   */
  public function access() {
    return user_access('access content');
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $form = parent::configure($form, $form_state);
    $form['block_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of topics'),
      '#default_value' => $this->configuration['block_count'],
      '#options' => drupal_map_assoc(array(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)));
    return $form;
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    parent::configureSubmit($form, $form_state);
    $this->configuration['block_count'] = $form_state['values']['block_count'];
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    $query = db_select('forum_index', 'f')
      ->fields('f')
      ->addTag('node_access')
      ->orderBy('f.created', 'DESC')
      ->range(0, $this->configuration['block_count']);

    return array(
      drupal_render_cache_by_query($query, 'forum_block_view'),
    );
  }
}
