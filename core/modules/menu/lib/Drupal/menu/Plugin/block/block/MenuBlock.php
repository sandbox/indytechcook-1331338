<?php

namespace Drupal\menu\Plugin\block\block;

use Drupal\system\Plugin\block\block\SystemMenuBlock;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "menu_menu_block",
 *   subject = @Translation("Menu"),
 *   module = "menu",
 *   derivative = "Drupal\menu\Plugin\Derivative\MenuBlock",
 *   settings = {
 *     "status" = "TRUE"
 *   }
 * )
 */
class MenuBlock extends SystemMenuBlock {
}
