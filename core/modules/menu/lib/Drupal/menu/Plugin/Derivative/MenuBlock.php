<?php

namespace Drupal\menu\Plugin\Derivative;

use Drupal\system\Plugin\Derivative\SystemMenuBlock;

class MenuBlock extends SystemMenuBlock {
  /**
   * Implements DerivativeInterface::getDerivativeDefinitions().
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    foreach (menu_get_menus(FALSE) as $menu => $name) {
      $this->derivatives[$menu] = $base_plugin_definition;
      $this->derivatives[$menu]['delta'] = $menu;
      $this->derivatives[$menu]['subject'] = t('Menu: @menu', array('@menu' => $name));
      $this->derivatives[$menu]['cache'] = DRUPAL_NO_CACHE;
    }
    return $this->derivatives;
  }
}
