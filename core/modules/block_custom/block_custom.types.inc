<?php

/**
 * @file
 * Block Type code
 */

/**
 * Get all of the block types.
 */
function block_custom_get_types() {
  $types = &drupal_static(__FUNCTION__);

  if (!isset($types)) {
    $types = array();
    foreach (module_implements('block_custom_type_info') as $module) {
      $module_types = module_invoke($module, 'block_custom_type_info');
      foreach ($module_types as $type => $info) {
        // Ensure default block type info properties.
        $info += array(
          'title' => $type,
          'description' => NULL,
          'module' => $module,
          'type' => $type,
          'settings' => array(),
        );
        // @todo Block types of one module are potentially overwriting block
        //   types of another module. Namespace info in $types somehow.
        $types[$type] = $info;
      }
    }
  }

  return $types;
}

/**
 * Build the URI for the block type
 */
function block_custom_type_uri($type) {
  return str_replace('_', '-', $type['type']);
}

