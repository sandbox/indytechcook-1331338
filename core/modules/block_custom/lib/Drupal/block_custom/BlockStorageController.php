<?php

/**
 * @file
 * Definition of Drupal\node\BlockStorageController.
 */

namespace Drupal\node;

use Drupal\entity\DatabaseStorageController;
use Drupal\entity\EntityInterface;
use Drupal\entity\EntityStorageException;
use Drupal\block_custom\Block;
use Exception;

class BlockStorageController extends DatabaseStorageController {
  /**
   * Overrides Drupal\entity\DatabaseStorageController::create().
   */
  public function create(array $values) {
    $block = parent::create($values);

    // Set the created time to now.
    if (empty($block->created)) {
      $block->created = REQUEST_TIME;
    }

    $this->changed = REQUEST_TIME;

    return $block;
  }

  /**
   * Set the delta function
   */
  protected function checkDelta(Block $block) {
    if (empty($block->delta)) {
      $max_length = 32;
      $delta = drupal_clean_css_identifier(strtolower($block->label));;

      $block->delta = substr($delta, 0, $max_length);

      // Check if delta is unique.
      if (block_custom_load_delta($block->delta)) {
        $i = 0;
        $separator = '-';
        $original_delta = $block->delta;
        do {
          $unique_suffix = $separator . $i++;
          $block->delta = substr($original_delta, 0, $max_length - drupal_strlen($unique_suffix)) . $unique_suffix;
        } while (block_custom_load_delta($block->delta));
      }
    }

    return $block;
  }

  protected function preSave(EntityInterface $entity) {
    $this->checkDelta($entity);
  }


}
