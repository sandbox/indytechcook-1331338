<?php

/**
 * @file
 * Definition of Drupal\block_custom\Block
 */

namespace Drupal\block_custom;

use Drupal\entity\Entity;

class Block extends Entity {
  /**
   * The Block ID
   *
   * @var integer
   */
  public $bid;

  /**
   * The Version ID
   *
   * @var integer
   */
  public $vid;

  /**
   * The node UUID.
   *
   * @var string
   */
  public $uuid;

  /**
   * The Block Type (bundle)
   *
   * @var string
   */
  public $type;

  /**
   * The block delta
   *
   * @var string
   */
  public $delta;

  /**
   * The labe in the admin interface
   *
   * @var string
   */
  public $label;

  /**
   * The description of the Block
   *
   * @var string
   */
  public $description;

  /**
   * The title of the Block
   *
   * @var string
   */
  public $title;

  /**
   * The node creation timestamp.
   *
   * @var integer
   */
  public $created;

  /**
   * The node modification timestamp.
   *
   * @var integer
   */
  public $changed;

  /**
   * The node owner's user ID.
   *
   * @var integer
   */
  public $uid;

  /**
   * Revisions for the block
   *
   * @var array
   */
  public $revisions = array();

  /**
   * Implements Drupal\entity\EntityInterface::id().
   */
  public function id() {
    return $this->bid;
  }

  /**
   * Implements Drupal\entity\EntityInterface::bundle().
   */
  public function bundle() {
    return $this->type;
  }


  /**
   * Overrides Drupal\entity\Entity::createDuplicate().
   */
  public function createDuplicate() {
    $duplicate = parent::createDuplicate();
    $duplicate->vid = NULL;
    return $duplicate;
  }

  /**
   * Overrides Drupal\entity\Entity::getRevisionId().
   */
  public function getRevisionId() {
    return $this->vid;
  }

  /**
   * Overrides Drupal\entity\Entity::uri().
   */
  public function uri() {
    if (!$uri = parent::uri()) {
      $uri = array(
        'path' => "block/{$this->id()}",
      );
    }

    return $uri;
  }

}
