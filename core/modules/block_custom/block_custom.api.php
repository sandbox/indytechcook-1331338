<?php

/**
 * @file
 * Hooks provided by the Block Custom module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define all Block Types
 *
 * This hook declares to Drupal what block types are provided by your module.
 *
 * In hook_block_type_info()
 */
function hook_block_type_info() {
  return array(
    'key' => array(
      'name' => 'machine_name',
      'label' => t('label'),
      'cache' => DRUPAL_CACHE_PER_ROLE,
      'editable' => FALSE, // Editable in the UI
    ),
  );
}

hook_block_type_{$block_type}_view();
hook_block_type_view_alter();
