<?php

namespace Drupal\node\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "node_syndicate_block",
 *   subject = @Translation("Syndicate"),
 *   module = "node",
 *   settings = {
 *     "status" = TRUE,
 *     "block_count" = "10",
 *     "cache" = DRUPAL_NO_CACHE
 *   }
 * )
 */
class SyndicateBlock extends BlockBase {
  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    return user_access('access content');
  }

  /**
   * Implements BlockInterface::build();
   */
  public function build() {
    return array(
      '#theme' => 'feed_icon',
      '#url' => 'rss.xml',
    );
  }
}
