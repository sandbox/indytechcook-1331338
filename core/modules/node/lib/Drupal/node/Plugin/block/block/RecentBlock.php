<?php

namespace Drupal\node\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "node_recent_block",
 *   subject = @Translation("Recent content"),
 *   module = "node",
 *   settings = {
 *     "status" = TRUE,
 *     "block_count" = "10"
 *   }
 * )
 */
class RecentBlock extends BlockBase {

  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    return user_access('access content');
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $form = parent::configure($form, $form_state);
    $form['block_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of recent content items to display'),
      '#default_value' => $this->configuration['block_count'],
      '#options' => drupal_map_assoc(array(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 30)),
    );
    return $form;
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    parent::configureSubmit($form, $form_state);
    $this->configuration['block_count'] = $form_state['values']['block_count'];
  }

  /**
   * Implements BlockInterface::build();
   */
  public function build() {
    if ($nodes = node_get_recent($this->configuration['block_count'])) {
      return array(
        '#theme' => 'node_recent_block',
        '#nodes' => $nodes,
      );
    }
    else {
      return array(
        '#children' => t('No content available.'),
      );
    }
  }
}
