<?php

namespace Drupal\comment\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *  id = "recent_comments",
 *  subject = @Translation("Recent comments"),
 *  module = "comment",
 *  settings = {
 *    "status" = TRUE,
 *    "block_count" = 10
 *  }
 * )
 */
class RecentBlock extends BlockBase {

  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    return user_access('access comments');
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $form = parent::configure($form, $form_state);
    $form['block_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of recent comments'),
      '#default_value' => $this->configuration['block_count'],
      '#options' => drupal_map_assoc(array(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 30)),
    );
    return $form;
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    parent::configureSubmit($form, $form_state);
    $this->configuration['block_count'] = $form_state['values']['block_count'];
  }

  /**
   * Implements BlockInterface::build();
   */
  public function build() {
    return array(
      '#theme' => 'comment_block',
      '#number' => $this->configuration['block_count'],
    );
  }
}
