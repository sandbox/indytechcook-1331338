<?php

namespace Drupal\aggregator\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DerivativeInterface;

class CategoryBlock implements DerivativeInterface {
  protected $derivatives;

  /**
   * Implements DerivativeInterface::getDerivativeDefinition().
   */
  public function getDerivativeDefinition($derivative_id, array $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $result = db_query('SELECT cid, title FROM {aggregator_category} ORDER BY title WHERE cid = :cid', array(':cid' => $derivative_id))->fetchObject();
    $this->derivatives[$derivative_id] = $base_plugin_definition;
    $this->derivatives[$derivative_id]['delta'] = $result->cid;
    $this->derivatives[$derivative_id]['subject'] = t('@title category latest items', array('@title' => $result->title));
    return $this->derivatives[$derivative_id];
  }

  /**
   * Implements DerivativeInterface::getDerivativeDefinitions().
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    $result = db_query('SELECT cid, title FROM {aggregator_category} ORDER BY title');
    foreach ($result as $category) {
      $this->derivatives[$category->cid] = $this->config;
      $this->derivatives[$category->cid]['delta'] = $category->cid;
      $this->derivatives[$category->cid]['subject'] = t('@title category latest items', array('@title' => $category->title));
    }
    return $this->derivatives;
  }
}
