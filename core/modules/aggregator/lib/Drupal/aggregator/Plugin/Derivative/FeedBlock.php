<?php

namespace Drupal\aggregator\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DerivativeInterface;

class FeedBlock implements DerivativeInterface {
  protected $derivatives;

  /**
   * Implements DerivativeInterface::getDerivativeDefinition().
   */
  public function getDerivativeDefinition($derivative_id, array $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $result = db_query('SELECT cid, title FROM {aggregator_category} ORDER BY title WHERE cid = :cid', array(':cid' => $derivative_id))->fetchObject();
    $this->derivatives[$derivative_id] = $base_plugin_definition;
    $this->derivatives[$derivative_id]['delta'] = $result->cid;
    $this->derivatives[$derivative_id]['subject'] = t('@title feed latest items', array('@title' => $result->title));
    return $this->derivatives[$derivative_id];
  }

  /**
   * Implements DerivativeInterface::getDerivativeDefinitions().
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    $result = db_query('SELECT fid, title FROM {aggregator_feed} WHERE block <> 0 ORDER BY fid');
    foreach ($result as $feed) {
      $this->derivatives[$feed->fid] = $this->config;
      $this->derivatives[$feed->fid]['delta'] = $feed->fid;
      $this->derivatives[$feed->fid]['subject'] = t('@title feed latest items', array('@title' => $feed->title));
    }
    return $this->derivatives;
  }
}
