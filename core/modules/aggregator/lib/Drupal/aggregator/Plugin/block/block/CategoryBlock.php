<?php

namespace Drupal\aggregator\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "aggregator_category_block",
 *   subject = @Translation("Aggregator category"),
 *   module = "aggregator",
 *   derivative = "Drupal\aggregator\Plugin\Derivative\CategoryBlock",
 *   settings = {
 *    "block_count" = "10",
 *    "status" = "TRUE"
 *   }
 * )
 */
class CategoryBlock extends BlockBase {
  protected $derivatives;
  protected $derivative;

  public function __construct(array $configuration, $plugin_id, DiscoveryInterface $discovery) {
    parent::__construct($configuration, $plugin_id, $discovery);
    $this->derivatives = array();
  }

  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    return user_access('access news feeds');
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $form = parent::configure($form, $form_state);
    $form['block_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of news items in block'),
      '#default_value' => $this->config['settings']['block_count'],
      '#options' => drupal_map_assoc(range(2, 20)),
    );
    return $form;
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    parent::configureSubmit($form, $form_state);
    $this->config['settings']['block_count'] = $form_state['values']['block_count'];
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    $id = $this->derivative;
    if ($category = db_query('SELECT cid, title, block FROM {aggregator_category} WHERE cid = :cid', array(':cid' => $id))->fetchObject()) {
      $result = db_query_range('SELECT i.* FROM {aggregator_category_item} ci LEFT JOIN {aggregator_item} i ON ci.iid = i.iid WHERE ci.cid = :cid ORDER BY i.timestamp DESC, i.iid DESC', 0, $this->config['settings']['block_count'], array(':cid' => $category->cid));
      $read_more = theme('more_link', array('url' => 'aggregator/categories/' . $category->cid, 'title' => t("View this category's recent news.")));

      $items = array();
      foreach ($result as $item) {
        $items[] = theme('aggregator_block_item', array('item' => $item));
      }

      // Only display the block if there are items to show.
      if (count($items) > 0) {
        return array(
          '#block' => $this->derivatives[$this->derivative],
          '#children' => theme('item_list', array('items' => $items)) . $read_more,
        );
      }
      return array();
    }
  }
}
