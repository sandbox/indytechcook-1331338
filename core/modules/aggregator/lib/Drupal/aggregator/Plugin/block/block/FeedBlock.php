<?php

namespace Drupal\aggregator\Plugin\block\block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * @Plugin(
 *   id = "aggregator_feed_block",
 *   subject = @Translation("Aggregator feed"),
 *   module = "aggregator",
 *   derivative = "Drupal\aggregator\Plugin\Derivative\FeedBlock",
 *   settings = {
 *    "block_count" = "10",
 *    "status" = "TRUE"
 *   }
 * )
 */
class FeedBlock extends BlockBase {
  protected $derivatives;
  protected $derivative;

  public function __construct() {
    $this->derivatives = array();
  }

  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    return user_access('access news feeds');
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure($form, &$form_state) {
    $form = parent::configure($form, $form_state);
    $form['block_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of news items in block'),
      '#default_value' => $this->config['block_count'],
      '#options' => drupal_map_assoc(range(2, 20)),
    );
    return $form;
  }

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {
    parent::configureSubmit($form, $form_state);
    $this->config['block_count'] = $form_state['values']['block_count'];
  }

  /**
   * Implements BlockInterface::build().
   */
  public function build() {
    $id = $this->derivative;
    if ($feed = db_query('SELECT fid, title, block FROM {aggregator_feed} WHERE block <> 0 AND fid = :fid', array(':fid' => $id))->fetchObject()) {
      $result = db_query_range("SELECT * FROM {aggregator_item} WHERE fid = :fid ORDER BY timestamp DESC, iid DESC", 0, $this->config['block_count'], array(':fid' => $id));
      $read_more = theme('more_link', array('url' => 'aggregator/sources/' . $feed->fid, 'title' => t("View this feed's recent news.")));

      $items = array();
      foreach ($result as $item) {
        $items[] = theme('aggregator_block_item', array('item' => $item));
      }

      // Only display the block if there are items to show.
      if (count($items) > 0) {
        return array(
          '#block' => $this->config,
          '#children' => theme('item_list', array('items' => $items)) . $read_more,
        );
      }
    }
  }
}
