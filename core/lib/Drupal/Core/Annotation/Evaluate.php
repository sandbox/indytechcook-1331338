<?php

/**
 * @file
 * Definition of Drupal\Core\Annotation\Evaluate.
 */

namespace Drupal\Core\Annotation;

use Drupal\Core\Annotation\AnnotationInterface;

/**
 * @Annotation
 */
class Evaluate implements AnnotationInterface {
  protected $code;

  public function __construct($values) {
    $this->code = $values['value'];
  }

  /**
   * Implements AnnotationInterface::get().
   */
  public function get() {
    return eval($this->code);
  }
}
