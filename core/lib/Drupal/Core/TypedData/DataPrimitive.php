<?php

/**
 * @file
 * Definition of Drupal\Core\TypedData\DataPrimitive.
 */

namespace Drupal\Core\TypedData;

/**
 * Class that holds constants for all primitive data types.
 */
final class DataPrimitive {

  /**
   * The BOOLEAN primitive data type.
   */
  const BOOLEAN = 1;

  /**
   * The STRING primitive data type.
   */
  const STRING = 2;

  /**
   * The INTEGER primitive data type.
   */
  const INTEGER = 3;

  /**
   * The DECIMAL primitive data type.
   */
  const DECIMAL = 4;

  /**
   * The DATE primitive data type.
   */
  const DATE = 5;

  /**
   * The DURATION primitive data type.
   */
  const DURATION = 6;

  /**
   * The URI primitive data type.
   */
  const URI = 7;

}
